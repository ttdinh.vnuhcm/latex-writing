\contentsline {chapter}{\numberline {1}Introduction}{13}
\contentsline {section}{\numberline {1.1}Hello world}{13}
\contentsline {section}{\numberline {1.2}Hello, world}{13}
\contentsline {section}{\numberline {1.3}Motivations for micro-optimization}{14}
\contentsline {section}{\numberline {1.4}Description of micro-optimization}{14}
\contentsline {subsection}{\numberline {1.4.1}Post Multiply Normalization}{15}
\contentsline {subsection}{\numberline {1.4.2}Block Exponent}{15}
\contentsline {section}{\numberline {1.5}Integer optimizations}{16}
\contentsline {subsection}{\numberline {1.5.1}Conversion to fixed point}{16}
\contentsline {subsection}{\numberline {1.5.2}Small Constant Multiplications}{17}
\contentsline {section}{\numberline {1.6}Other optimizations}{18}
\contentsline {subsection}{\numberline {1.6.1}Low-level parallelism}{18}
\contentsline {subsection}{\numberline {1.6.2}Pipeline optimizations}{19}
\contentsline {chapter}{\numberline {A}Tables}{21}
\contentsline {chapter}{\numberline {B}Figures}{23}
