\beamer@sectionintoc {1}{Introduction}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{History of Java}{5}{0}{1}
\beamer@subsectionintoc {1}{2}{What is Java}{6}{0}{1}
\beamer@subsectionintoc {1}{3}{Java Editions}{7}{0}{1}
\beamer@subsectionintoc {1}{4}{Java Platform}{8}{0}{1}
\beamer@subsectionintoc {1}{5}{Java Development}{9}{0}{1}
