\beamer@sectionintoc {1}{Inehritance}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{Inheritance}{5}{0}{1}
\beamer@subsectionintoc {1}{2}{Overriding}{6}{0}{1}
\beamer@subsectionintoc {1}{3}{Abstract class}{7}{0}{1}
\beamer@subsectionintoc {1}{4}{Object Class}{8}{0}{1}
\beamer@sectionintoc {2}{Package, Interface and Exception}{9}{0}{2}
\beamer@subsectionintoc {2}{1}{Package}{10}{0}{2}
\beamer@subsectionintoc {2}{2}{Interface}{11}{0}{2}
\beamer@subsectionintoc {2}{3}{Exception}{12}{0}{2}
\beamer@sectionintoc {3}{Enumeration, TypeWrappers and Autoboxing}{13}{0}{3}
\beamer@subsectionintoc {3}{1}{Enumeration}{14}{0}{3}
\beamer@subsectionintoc {3}{2}{Type Wrappers}{15}{0}{3}
\beamer@subsectionintoc {3}{3}{Auto(boxing/unboxing)}{16}{0}{3}
\beamer@sectionintoc {4}{Thread}{17}{0}{4}
\beamer@subsectionintoc {4}{1}{Multitasking}{18}{0}{4}
\beamer@subsectionintoc {4}{2}{Main Thread}{19}{0}{4}
\beamer@subsectionintoc {4}{3}{Thread Pool}{20}{0}{4}
\beamer@subsectionintoc {4}{4}{Synchronization}{21}{0}{4}
\beamer@sectionintoc {5}{Networking}{22}{0}{5}
\beamer@subsectionintoc {5}{1}{TCP}{23}{0}{5}
\beamer@subsectionintoc {5}{2}{UDP}{23}{0}{5}
