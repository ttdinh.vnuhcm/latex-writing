\beamer@sectionintoc {1}{Introduction}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{History of Java}{5}{0}{1}
\beamer@subsectionintoc {1}{2}{What is Java}{6}{0}{1}
\beamer@subsectionintoc {1}{3}{Java Editions}{7}{0}{1}
\beamer@subsectionintoc {1}{4}{Java Platform}{8}{0}{1}
\beamer@subsectionintoc {1}{5}{Java Development}{9}{0}{1}
\beamer@sectionintoc {2}{More Details}{9}{0}{2}
\beamer@subsectionintoc {2}{1}{Arrays}{10}{0}{2}
\beamer@subsectionintoc {2}{2}{Command Line Arguments}{11}{0}{2}
\beamer@subsectionintoc {2}{3}{For-Each}{12}{0}{2}
\beamer@subsectionintoc {2}{4}{Scanner}{13}{0}{2}
\beamer@subsectionintoc {2}{5}{Static}{14}{0}{2}
\beamer@subsectionintoc {2}{6}{Nested and Inner Classes}{15}{0}{2}
\beamer@sectionintoc {3}{Strings}{16}{0}{3}
\beamer@subsectionintoc {3}{1}{String}{17}{0}{3}
\beamer@subsectionintoc {3}{2}{String Buffer}{17}{0}{3}
\beamer@subsectionintoc {3}{3}{String Tokenizer}{17}{0}{3}
\beamer@sectionintoc {4}{Inehritance}{17}{0}{4}
